<?php 
	//Interfaz de los patos
	interface Pato 
	{
		public function graznar();
		public function volar();
	}

	//Se implementan los métodos de la interfaz
	class PatoPekin implements Pato 
	{
		public function graznar() 
		{
			print "Graznando..."."<br>"; 
		}

		public function volar() 
		{
			print "Volando..."."<br>"; 
		}
	}

	//Interfaz de las gallinas
	interface Gallina 
	{
		public function cacarear();
		public function volar();
	}

	//Se implementan los métodos de la interfaz
	class GallinaLeghorn implements Gallina 
	{
		public function cacarear() 
		{
			print "Cacareando..."."<br>"; 
		}

		public function volar() 
		{
			print "Volando poquito..."."<br>"; 
		}
	}

	//Se implementa la interfaz del tipo que se quiere adaptar
	class GallinaAdapter implements Pato 
	{
		private $gallina;

		//En el constructor se obtiene la referencia del objeto a adaptar
		public function __construct(Gallina $gallina) 
		{
			$this->gallina = $gallina;
		}

		//Se implementan los métodos de la interfaz del pato
		//El método graznar se traduce al método de cacarear
		public function graznar() 
		{
			$this->gallina->cacarear();
		}

		//La gallina y el pato vuelan, el problema es la distancia
		//por eso se repite el método de volar de la gallina
		public function volar() 
		{
			$this->gallina->volar();
			$this->gallina->volar();
			$this->gallina->volar();
		}
	}

	print "<br>"."Pato de Pekin"."<br>";
	//Se crea una instancia del objeto PatoPekin
	$pato=new PatoPekin();
	$pato->graznar();
	$pato->volar();
	//Se comprueba si es un pato o no
	probarPato($pato);
	print "<br>"."Gallina Leghorn"."<br>";
	//Se crea una instancia del objeto GallinaLeghorn
	$gallina=new GallinaLeghorn();
	$gallina->cacarear();
	$gallina->volar();
	//Se comprueba si es un pato o no
	probarPato($gallina);
	print "<br>"."Pato Gallina"."<br>";
	//Se crea una instancia del objeto GallinaAdapter
	$patoGallina=new GallinaAdapter($gallina);
	$patoGallina->graznar();
	$patoGallina->volar();
	//Se comprueba si es un pato o no
	probarPato($patoGallina);
	
	//Comprueba si es un objeto de tipo Pato o no
	function probarPato($ave) {
		if ($ave instanceof Pato) {
			echo "Es un pato"."<br>";
		} else {
			echo "No es un pato"."<br>";
		}
	}
?>