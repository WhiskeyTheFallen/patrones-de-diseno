<?php 
	interface Libro
	{
		public function pasarPagina();
		public function abrir();
		public function getPagina(): int;
	}

	class libroPapel implements Libro
	{
		private $pagina;

		public function abrir()
		{
			$this->pagina = 1;
		}

		public function pasarPagina()
		{
			$this->pagina++;
		}

		public function getPagina(): int
		{
			return $this->pagina;
		}
	}

	interface EBook
	{
		public function desbloquear();
		public function presionarSiguiente();
		public function getPagina(): array;
	}

	class Kindle implements EBook
	{
		private $pagina = 1;
		private $totalPaginas = 100;

		public function presionarSiguiente()
		{
			$this->pagina++;
		}

		public function desbloquear()
		{
		}

		public function getPagina(): array
		{
			return [$this->pagina, $this->totalPaginas];
		}
	}
?>