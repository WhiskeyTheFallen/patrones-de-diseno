<?php 
	//Interfaz de los patos
	interface Pato 
	{
		public function graznar();
		public function volar();
	}

	//Se implementan los métodos de la interfaz
	class PatoPekin implements Pato 
	{
		public function graznar() 
		{
			print "Graznando..."."<br>"; 
		}

		public function volar() 
		{
			print "Volando..."."<br>"; 
		}
	}

	//Interfaz de las gallinas
	interface Gallina 
	{
		public function cacarear();
		public function volar();
	}

	//Se implementan los métodos de la interfaz
	class GallinaLeghorn implements Gallina 
	{

		public function cacarear() 
		{
			print "Cacareando..."."<br>"; 
		}

		public function volar() 
		{
			print "Volando poquito..."."<br>"; 
		}
	}

	//Se implementa la interfaz del tipo que se quiere adaptar y
	//se hereda de la clase que se va a modificar
	class GallinaAdapter extends GallinaLeghorn implements Pato 
	{
		//Se implementan los métodos de la interfaz del pato
		//El método graznar se traduce al método de cacarear
		//heredado de la clase GallinaLeghorn
		public function graznar() 
		{
			$this->cacarear();
		}

		//La gallina y el pato vuelan, el problema es la distancia
		//por eso se repite el método de volar heredado por la clase GallinaLeghorn
		public function volar() 
		{
			parent::volar();
			parent::volar();
			parent::volar();
		}
	}

	print "<br>"."Pato de Pekin"."<br>";
	//Se crea una instancia del objeto PatoPekin
	$pato=new PatoPekin();
	$pato->graznar();
	$pato->volar();
	//Se comprueba si es un pato o no
	probarPato($pato);
	print "<br>"."Gallina Leghorn"."<br>";
	//Se crea una instancia del objeto GallinaLeghorn
	$gallina=new GallinaLeghorn();
	$gallina->cacarear();
	$gallina->volar();
	//Se comprueba si es un pato o no
	probarPato($gallina);
	print "<br>"."Pato Gallina"."<br>";
	//Se crea una instancia del objeto GallinaAdapter
	$patoGallina=new GallinaAdapter();
	$patoGallina->graznar();
	$patoGallina->volar();
	//Se comprueba si es un pato o no
	probarPato($patoGallina);
	
	//Comprueba si es un objeto de tipo Pato o no
	function probarPato($ave) {
		if ($ave instanceof Pato) {
			echo "Es un pato"."<br>";
		} else {
			echo "No es un pato"."<br>";
		}
	}
?>