<?php
    //Clase calculadora
    class Calculadora
    {
        public function suma(float $a, float $b) : float
        {
            return $a+$b;
        }

        public function resta(float $a, float $b) : float
        {
            return $a-$b;
        }

        public function multiplicacion(float $a, float $b) : float
        {
            return $a*$b;
        }

        public function division(float $a, float $b) : float
        {
            return $a/$b;
        }
    }

    //Se crea la Calculadora
    $calculadora=new Calculadora();
    //Si se quiere hacer una suma
    print "La suma es: ".$calculadora->suma(5, 10)."<br>";
    //Si se quiere hacer una resta
    print "La resta es: ".$calculadora->resta(5, 10)."<br>";
    //Si se quiere hacer una multiplicación
    print "La multiplicación es: ".$calculadora->multiplicacion(5, 10)."<br>";
    //Si se quiere hacer una división
    print "La división es: ".$calculadora->division(5, 10)."<br>";
?>