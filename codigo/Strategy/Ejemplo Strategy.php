<?php
    //Interfaz Strategy declara un método común para todas las estrategias
	interface RutaStrategy 
	{
		public function crearRuta(string $origen, string $destino) : string;
	}

    //Estrategia concreta
    class CarreteraStrategy implements RutaStrategy
    {
        public function crearRuta(string $origen, string $destino) : string
        {
            return "Se creó una ruta por carretera de ".$origen. " a ".$destino;
        }
    }
    
    //Estrategia concreta
    class TransportePublicoStrategy implements RutaStrategy
    {
        public function crearRuta(string $origen, string $destino) : string
        {
            return "Se creó una ruta por transporte público de ".$origen. " a ".$destino;
        }
    }

    //Estrategia concreta
    class PedestreStrategy implements RutaStrategy
    {
        public function crearRuta(string $origen, string $destino) : string
        {
            return "Se creó una ruta para peatones de ".$origen. " a ".$destino;
        }
    }

    //La clase contexto define la interfaz de interes a los clientes
    class Navegador
    {
        //Variable que ayuda a mantener una referencia a todas las estrategias
        private $strategy;

        //El constructor necesita de una estrategia para usarse
        public function __construct(RutaStrategy $strategy)
        {
            $this->strategy=$strategy;
        }

        //Método que permite cambiar de estrategia
        public function setEstrategia(RutaStrategy $strategy)
        {
            $this->strategy=$strategy;
        }

        //Método que crea una ruta a partir de la estrategia
        public function crearRuta(string $origen, string $destino)
        {
            print $this->strategy->crearRuta($origen, $destino)."<br>";
        }
    }

    //Si se quiere hacer una ruta a para autos
    //Se crea el Navegador pasandole la estrategia que queremos
    $navegador=new Navegador(new CarreteraStrategy());
    $navegador->crearRuta("Ciudad de México", "Monterrey");
    //Si se quiere hacer una ruta para transporte público
    $navegador->setEstrategia(new TransportePublicoStrategy());
    $navegador->crearRuta("Ciudad de México", "Monterrey");
    //Si se quiere hacer una ruta para peatones
    $navegador->setEstrategia(new PedestreStrategy());
    $navegador->crearRuta("Ciudad de México", "Monterrey");
?>