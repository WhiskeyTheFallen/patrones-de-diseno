<?php
    class FacadeProducto
    {
        private $productos = [];
        private $archivo;

        public function __construct(string $archivo)
        {
            $this->archivo = $archivo;
            $this->compilar();
        }

        private function compilar()
        {
            $lineas = $this->getLineasArchivo($this->archivo);
            foreach ($lineas as $linea) {
                $id = $this->getIdLinea($linea);
                $nombre = $this->getNombreLinea($linea);
                $this->productos[$id] = $this->getObjetoProductoId($id, $nombre);
            }
        }

        private function getLineasArchivo($archivo)
        {
            return file($archivo);
        }

        private function getObjetoProductoId($id, $nombreProducto)
        {
            //Algún tipo de búsqueda en la base de datos
            return new Producto($id, $nombreProducto);
        }

        private function getNombreLinea($linea)
        {
            if (preg_match("/.*-(.*)\s\d+/", $linea, $arreglo)) {
                return str_replace('_', ' ', $arreglo[1]);
            }
            return '';
        }

        private function getIdLinea($linea)
        {
            if (preg_match("/^(\d{1,3})-/", $linea, $arreglo)) {
                return $arreglo[1];
            }
            return -1;
        }

        public function getProductos(): array
        {
            return $this->productos;
        }

        public function getProducto(string $id): \Producto
        {
            if (isset($this->productos[$id])) {
                return $this->productos[$id];
            }
            return null;
        }
    }

    class Producto
    {
        public $id;
        public $nombre;

        public function __construct($id, $nombre)
        {
            $this->id = $id;
            $this->nombre = $nombre;
        }
    }

    //Solo es necesario crear un objeto del Facade para obtener lo que necesitamos
    $facade = new FacadeProducto(__DIR__ . '/Texto.txt');
    $objecto = $facade->getProducto("234");
    $objectos = $facade->getProductos();

    print_r($objecto);
    echo "<br>";
    print_r($objectos);
?>