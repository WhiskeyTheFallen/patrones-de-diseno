<?php
    //Clase que representa un sistema operativo
    class SistemaOperativo
    {
        public function iniciar()
        {
            print "Iniciando el SO..."."<br>"; 
        }
        
        public function detener()
        {
            print "Deteniendo el SO..."."<br>"; 
        }        
    }

    //Clase que representa la bios
    class Bios
    {
        public function ejecutar()
        {
            print "Ejecutando la BIOS..."."<br>"; 
        }

        public function iniciar(SistemaOperativo $so)
        {
            print "Iniciando BIOS..."."<br>"; 
            $so->iniciar();
        }

        public function apagar()
        {
            print "Apagando BIOS..."."<br>"; 
        }
    }

    $bios = new Bios();
    $so = new SistemaOperativo();
    //Iniciando la BIOS y el sistema operativo
    $bios->ejecutar();
    $bios->iniciar($so);
    //Apaagando la BIOS y el sistema operativo
    $so->detener();
    $bios->apagar();

?>