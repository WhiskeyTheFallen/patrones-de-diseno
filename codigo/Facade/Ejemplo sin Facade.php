<?php
    //Función que devuelve un arreglo con el contenido de un cierto archivo
    function getLineasArchivo($archivo)
    {
        return file($archivo);
    }

    //Función que devuelve un objeto tipo producto
    function getObjetoProductoId($id, $nombreProducto)
    {
        //Algún tipo de búsqueda en la base de datos
        return new Producto($id, $nombreProducto);
    }

    //Función que devuelve el nombre que se encuentra en una línea del archivo
    function getNombreLinea($linea)
    {
        if (preg_match("/.*-(.*)\s\d+/", $linea, $arreglo)) {
            return str_replace('_', ' ', $arreglo[1]);
        }
        return '';
    }

    //Función que devuelve el id que se encuentra en una línea del archivo
    function getIdLinea($linea)
    {
        if (preg_match("/^(\d{1,3})-/", $linea, $arreglo)) {
            return $arreglo[1];
        }
        return -1;
    }

    class Producto
    {
        public $id;
        public $nombre;

        public function __construct($id, $nombre)
        {
            $this->id = $id;
            $this->nombre = $nombre;
        }
    }

    $lineas = getLineasArchivo(__DIR__ . '/Texto.txt');
    $objetos = [];
    foreach ($lineas as $linea) {
        $id = getIdLinea($linea);
        $name = getNombreLinea($linea);
        $objetos[$id] = getObjetoProductoId($id, $name);
    }
    print_r($objetos);
?>