<?php
    //Clase Creador, declara el método Factory
    abstract class Logistica
    {
        //Lógica de negocio
        public function planEntrega()
        {
            $transporte=$this->crearTransporte();
            print "Tu paquete se entregara en ".$transporte->entrega();
        }

        //Método Factory, hay que asegurarse que devuelve un objeto de
        //la clase del producto
        abstract public function crearTransporte() : \Transporte;
    }

    //Clase Creador Concreto, implementa el método Factory
    class LogisticaTierra extends Logistica
    {
        //Método Factory, hay que asegurarse que devuelve un objeto de
        //la clase del producto, en este caso un camión
        public function crearTransporte() : \Transporte
        {
            return new Camion();
        }
    }

    //Clase Creador Concreto, implementa el método Factory
    class LogisticaMar extends Logistica
    {
        //Método Factory, hay que asegurarse que devuelve un objeto de
        //la clase del producto, en este caso un barco
        public function crearTransporte() : \Transporte
        {
            return new Barco();
        }
    }

    //Interfaz del Producto
	interface Transporte 
	{
		public function entrega() : string;
    }
    
    //Clase de Producto Concreto
    class Camion implements Transporte
    {
        //Implementa el método de la interfaz
        public function entrega() : string
        {
            return "Camión"."<br>";
        }
    }

    //Clase de Producto Concreto
    class Barco implements Transporte
    {
        //Implementa el método de la interfaz
        public function entrega() : string
        {
            return "Barco"."<br>";
        }
    }

    //El usuario elige un Creador Concreto, dependiendo de sus necesidades
    //Se crea un objeto de tipo LogisticaTierra
    $transporte=new LogisticaTierra();
    $transporte->planEntrega();
    //Se crea un objeto de tipo LogisticaTierra
    $transporte=new LogisticaMar();
    $transporte->planEntrega();  
?>