<?php
    //Clase que dibuja una figura
    class Figuras
    {
        //Método que dibuja un Círculo
        public function dibujarCirculo()
        {
            $circulo=new Circulo();
            print "Se dibuja un ".$circulo->getNombre();
        }

        //Método que dibuja un Triángulo
        public function dibujarTriangulo()
        {
            $triangulo=new Triangulo();
            print "Se dibuja un ".$triangulo->getNombre();
        }

        //Método que dibuja un Cuadrado
        public function dibujarCuadrado()
        {
            $cuadrado=new Cuadrado();
            print "Se dibuja un ".$cuadrado->getNombre();
        }
    }

    //Clase de Circulo
    class Circulo
    {
        public function getNombre() : string
        {
            return "Círculo"."<br>";
        }
    }

    //Clase de Triangulo
    class Triangulo
    {
        public function getNombre() : string
        {
            return "Triángulo"."<br>";
        }
    }

    //Clase de Cuadrado
    class Cuadrado
    {
        public function getNombre() : string
        {
            return "Cuadrado"."<br>";
        }
    }

    //Se dibuja un Círculo
    $figuras=new Figuras();
    $figuras->dibujarCirculo();
    //Se dibuja un Triángulo
    $figuras->dibujarTriangulo();
    //Se dibuja un Cuadrado
    $figuras->dibujarCuadrado(); 
?>