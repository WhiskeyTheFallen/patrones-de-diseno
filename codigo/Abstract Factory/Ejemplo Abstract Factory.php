<?php
    /*Abstract Factory, declara métodos que devuelven Productos Abstractos
    Estos productos son parte de una misma familia.*/
    interface MueblesFactory
    {
        public function crearSilla() : \Silla;
        public function crearMesaCafe() : \MesaCafe;
    }

    //Factory Concreta crea una familia de productos de una misma variante
    class FactoryModerna implements MueblesFactory
    {
        public function crearSilla() : \Silla
        {
            return new SillaModerna();
        }

        public function crearMesaCafe() : \MesaCafe
        {
            return new MesaCafeModerna();
        }
    }

    //Factory Concreta crea una familia de productos de una misma variante
    class FactoryVictoriana implements MueblesFactory
    {
        public function crearSilla() : \Silla
        {
            return new SillaVictoriana();
        }
        
        public function crearMesaCafe() : \MesaCafe
        {
            return new MesaCafeVictoriana();
        }
    }

    //Producto Abstracto
    interface Silla
    {
        public function sentarse();
    }

    //Producto Concreto creado por una Factory Concreta
    class SillaVictoriana implements Silla
    {
        public function sentarse()
        {
            print "Sentándose en una Silla Victoriana"."<br>";
        }
    }

    //Producto Concreto creado por una Factory Concreta
    class SillaModerna implements Silla
    {
        public function sentarse()
        {
            print "Sentándose en una Silla Moderna"."<br>";
        }
    }

    //Producto Abstracto
    interface MesaCafe
    {
        public function tomarCafe();
    }

    //Producto Concreto creado por una Factory Concreta
    class MesaCafeVictoriana implements MesaCafe
    {
        public function tomarCafe()
        {
            print "Tomando una taza de café de una Mesa de Café Victoriana"."<br>";
        }
    }

    //Producto Concreto creado por una Factory Concreta
    class MesaCafeModerna implements MesaCafe
    {
        public function tomarCafe()
        {
            print "Tomando una taza de café de una Mesa de Café Moderna"."<br>";
        }
    }

    //El usuario elige una Factory Concreta, dependiendo de sus necesidades
    $factoryModerna=new FactoryModerna();
    //Se llama a un método para que cree los muebles y se usen
    crearMuebles($factoryModerna);
    //El usuario elige una Factory Concreta, dependiendo de sus necesidades
    $factoryVictoriana=new FactoryVictoriana();
    //Se llama a un método para que cree los muebles y se usen
    crearMuebles($factoryVictoriana);

    function crearMuebles(MueblesFactory $factory)
    {
        $silla=$factory->crearSilla();
        $silla->sentarse();
        $mesaCafe=$factory->crearMesaCafe();
        $mesaCafe->tomarCafe();
    }
?>