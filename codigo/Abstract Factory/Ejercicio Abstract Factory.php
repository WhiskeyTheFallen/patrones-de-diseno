<?php
    //Clase Restaurante, tiene dos platillos Pizza y Hamburguesas
    class Restaurante
    {
        //Método que prepara una Pizza
        public function prepararPizza(string $tipo)
        {
            $pizza=new Pizza();
            if ($tipo == "Queso") {
                $pizza->prepararPizzaQueso();
            } else if ($tipo == "Pepperoni") {
                $pizza->prepararPizzaPepperoni();
            }
        }
        //Método que prepara una Hamburguesa
        public function prepararHamburguesa(string $tipo)
        {
            $hamburguesa=new Hamburguesa();
            if ($tipo == "Ranchera") {
                $hamburguesa->prepararHamburguesaRanchera();
            } else if ($tipo == "Vegetariana") {
                $hamburguesa->prepararHamburguesaVegetariana();
            }
        }
    }

    //Clase de Pizza
    class Pizza
    {
        public function prepararPizzaQueso()
        {
            print "Se preparó una Pizza de Queso"."<br>";
        }

        public function prepararPizzaPepperoni()
        {
            print "Se preparó una Pizza de Pepperoni"."<br>";
        }
    }

    //Clase de Hamburguesa
    class Hamburguesa
    {
        public function prepararHamburguesaRanchera()
        {
            print "Se preparó una Hamburguesa Ranchera"."<br>";
        }

        public function prepararHamburguesaVegetariana()
        {
            print "Se preparó una Hamburguesa Vegetariana"."<br>";
        }
    }

    //Se crea una clase Restaurante
    $restaurante=new Restaurante();
    //Se prepara una Pizza de Queso
    $restaurante->prepararPizza("Queso");
    //Se prepara una Pizza de Pepperoni
    $restaurante->prepararPizza("Pepperoni");
    //Se prepara una Hamburguesa Ranchera
    $restaurante->prepararHamburguesa("Ranchera");
    //Se prepara una Hamburguesa Vegetariana
    $restaurante->prepararHamburguesa("Vegetariana");
?>