<?php 
	class Preferencias
	{
		private $config = [];

		/*Se usa una variable privada y estática para guardar la instancia del objeto ya que no se quiereque 
		se pueda acceder fuera de la clase y además se quiere que esta variable sea de la clase no del objeto*/
		private static $instancia; 
	
		//Constructor privado que impide que no se pueda crear un objeto de la clase fuera de ella misma
		private function __construct()
		{
		}

		/*Función que verifica si existe una instancia de un objeto y si no existe la crea, es pública y estática
		ya que se quiere que cualquiera pueda llamar a este método por media de la clase*/
		public static function getInstancia()
		{
			if (empty(self::$instancia)) {
				self::$instancia = new Preferencias();
			}
			return self::$instancia;
		}

		public function setPropiedad(string $key, string $val)
		{
			$this->config[$key] = $val;
		}

		public function getPropiedad(string $key): string
		{
			return $this->config[$key];
		}

	}

	//Se crea una instancia
	$pref = Preferencias::getInstancia();
	$pref->setPropiedad("nombre", "Fulano");
	//Se remueve la instancia que se creo
	unset($pref); 
	//Se crea una nueva instancia
	$pref2 = Preferencias::getInstancia();
	//Se comprueba que la propiedad se conserva, es decir, se está usando la misma instancia del objeto
	print $pref2->getPropiedad("nombre"); 
