
<?php 
    class Singleton 
    {
        /*Se usa una variable privada y estática para guardar la instancia del objeto ya que no se quiereque 
        se pueda acceder fuera de la clase y además se quiere que esta variable sea de la clase no del objeto*/
        private static $instancia; 

        //Constructor privado que impide que no se pueda crear un objeto de la clase fuera de ella misma
        private function __construct()
        {
        }

        public static function getInstancia()
        {
            if (empty(self::$instancia)) {
                self::$instancia = new Singleton();
            }
            return self::$instancia;
        }

        //Más métodos que se quieran
    }
?>