<?php
	class CalderaChocolate
	{
		private $calderaVacia;
		private $calderaHirviendo;

		public function __construct()
		{
			$this->calderaVacia = true;
			$this->calderaHirviendo = false;
		}

		public function llenarCaldera() : string
		{
			if ($this->getCalderaVacia()) {
				//Se llena la caldera con una mezcla de leche / chocolate
				$this->calderaVacia = false;
				return "Caldera llena.";
			}
			return "Caldera ya está llena.";
		}

		public function drenarCaldera() : string
		{
			if (!$this->getCalderaVacia() && $this->getCalderaHirviendo()) {
				//Drenar la leche hirviendo y el chocolate
				$this->calderaVacia = true;
				$this->calderaHirviendo = false;
				return "Caldera vacía.";
			}
			return "Caldera vacía o sin hervir.";
		}

		public function hervirCaldera() : string
		{
			if (!$this->getCalderaVacia() && !$this->getCalderaHirviendo()) {
				//Hervir el contenido de la caldera
				$this->calderaHirviendo = true;
				return "Caldera hirviendo.";
			}
			return "Caldera vacía o ya está hirviendo.";
		}

		public function getCalderaVacia() 
		{
			return $this->calderaVacia;
		}

		public function getCalderaHirviendo() 
		{
			return $this->calderaHirviendo;
		}
	}

	//Se crea una instancia de la caldera
	$caldera = new CalderaChocolate();
	print $caldera->llenarCaldera() ."<br>"; 
	print $caldera->hervirCaldera() ."<br>"; 
	//Se crea una nueva instancia de la caldera, y ambas instancias tienen estados diferentes
	$caldera1 = new CalderaChocolate();
	print $caldera1->llenarCaldera() ."<br>";
?>